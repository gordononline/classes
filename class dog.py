# attributes have self.
# methods have def ....:


class Dog(object):
    """defines dog class"""
    def __init__(self, name, age):
        self.name=name
        self.age=age
              
    def speak(self):
        """defines print function"""
        print(f"Nice dog {self.name} you are {self.age} years old")
        
    def talk(self):
        print("Bark")
 
class Cat(Dog):
    def __init__(self, name, age, color):
            super().__init__(name, age)
            self.color = color
            
    def talk(self):
        print("Meow")
  
   
joe=Dog("Fido",9) 
    
tim = Cat("Blunt", 7,"red")
joe.talk()
joe.speak()
tim.talk()
tim.speak()
tim.talk()

